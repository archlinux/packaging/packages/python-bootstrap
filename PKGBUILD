# Maintainer

pkgbase=python-bootstrap
pkgname=(python-{build,flit-core,installer,packaging,pyproject-hooks,setuptools,wheel})
pkgver=0
pkgrel=1
arch=(any)
license=('custom')
makedepends=(python git)
url="https://gitlab.archlinux.org/archlinux/python-bootstrap"
source=(
  "${pkgbase}::git+https://gitlab.archlinux.org/archlinux/python-bootstrap.git#branch=python-3.12"
  "${pkgbase}-build::git+https://github.com/pypa/build.git"
  "${pkgbase}-flit::git+https://github.com/takluyver/flit.git"
  "${pkgbase}-installer::git+https://github.com/pypa/installer.git"
  "${pkgbase}-wheel::git+https://github.com/pypa/wheel.git"
  "${pkgbase}-packaging::git+https://github.com/pypa/packaging"
  "${pkgbase}-pyproject-hooks::git+https://github.com/pypa/pyproject-hooks"
  "${pkgbase}-setuptools::git+https://github.com/pypa/setuptools.git"
)
sha256sums=('SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP'
            'SKIP')

prepare() {
  cd python-bootstrap
  git submodule init

  git config submodule."external/build".url "${srcdir}/${pkgbase}"-build
  git config submodule."external/flit".url "${srcdir}/${pkgbase}"-flit
  git config submodule."external/installer".url "${srcdir}/${pkgbase}"-installer
  git config submodule."external/wheel".url "${srcdir}/${pkgbase}"-wheel
  git config submodule."external/packaging".url "${srcdir}/${pkgbase}"-packaging
  git config submodule."external/pyproject-hooks".url "${srcdir}/${pkgbase}"-pyproject-hooks
  git config submodule."external/setuptools".url "${srcdir}/${pkgbase}"-setuptools

  git -c protocol.file.allow=always submodule update
  git submodule update --init --recursive
}

build() {
  cd python-bootstrap
  python -m bootstrap.build
}

package_python-build() {
  pkgdesc="A simple, correct PEP 517 build frontend"
  depends=(
    python
    python-packaging
    python-pyproject-hooks
  )

  cd python-bootstrap
  python -m bootstrap.install dist/build-*-py3-none-any.whl -d "$pkgdir"
}

package_python-installer() {
  pkgdesc="Low-level library for installing a Python package from a wheel distribution"
  depends=(python)

  cd python-bootstrap
  python -m bootstrap.install dist/installer-*-py3-none-any.whl -d "$pkgdir"
}

package_python-flit-core() {
  pkgdesc="Simplified packaging of Python modules (core backend)"
  depends=(python)

  cd python-bootstrap
  python -m bootstrap.install dist/flit_core-*-py3-none-any.whl -d "$pkgdir"
}

package_python-wheel() {
  pkgdesc="A built-package format for Python"
  depends=(python)

  cd python-bootstrap
  python -m bootstrap.install dist/wheel-*-py3-none-any.whl -d "$pkgdir"
}

package_python-packaging() {
  pkgdesc="Core utilities for Python packages"
  depends=(python)

  cd python-bootstrap
  python -m bootstrap.install dist/packaging-*-py3-none-any.whl -d "$pkgdir"
}

package_python-pyproject-hooks() {
  pkgdesc="A low-level library for calling build-backends in pyproject.toml-based project"
  depends=(python)

  cd python-bootstrap
  python -m bootstrap.install dist/pyproject_hooks-*-py3-none-any.whl -d "$pkgdir"
}

package_python-setuptools() {
  pkgdesc="Setuptools (bootstrapped version)"
  depends=(python)

  cd python-bootstrap
  python -m bootstrap.install dist/setuptools-*-py3-none-any.whl -d "$pkgdir"
}
